//
//  MyTabbarController.swift
//  Swift_demo
//
//  Created by csl on 2018/2/28.
//  Copyright © 2018年 csl. All rights reserved.
//

import UIKit

class MyTabbarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        initVc();
        
        
    }

    
     func initVc() -> Void {
        

        var classNameArr = ["Home","Listen","Found","Me"];
        var imageNames=["home","hear","find","mine"];
        //var titleNameArr = ["首页","我听","发现","我的"];
        
        
        var tabArr: [UIViewController] = []
        let projectName = self.getProjectName()
      
        for i in 0 ..< classNameArr.count
        {
            let clsName = classNameArr[i]
            //let lowStr = clsName.lowercased()
            
            let clsType = NSClassFromString(projectName+clsName+"ViewController") as! UIViewController.Type
            let vc = clsType.init()
           // vc.title = titleNameArr[i]
            
            let imageName = imageNames[i];
            
            
            let image = UIImage(named: "tabbar_icon_" + imageName + "_normal")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
            
            let selectedImage = UIImage(named: "tabbar_icon_" + imageName + "_pressed")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
            
            
            vc.tabBarItem.image=image;
            vc.tabBarItem.selectedImage=selectedImage;
            
            
            vc.tabBarItem.imageInsets=UIEdgeInsets(top: 0, left: 0, bottom: -8, right: 0);
            
            tabArr.append(vc);
            
        }
        
        let placeVC = UIViewController()
        placeVC.view.backgroundColor = kThemeWhiteColor
        
        tabArr.insert(placeVC, at: 2)
        
        self.viewControllers = tabArr
        

    }
    
    
    private func getProjectName() -> String {
        guard let infoDict = Bundle.main.infoDictionary else {
            return "."
        }
        let key = kCFBundleExecutableKey as String
        guard let value = infoDict[key] as? String else {
            return "."
        }
        return value + "."
    }
    
    



}
